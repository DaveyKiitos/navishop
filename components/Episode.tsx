import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import axios from 'axios'

export default function Episodes({route, navigation}) {
  const episodeId = route.params?.episodeId || "tt2527338";
  const [episode, setEpisode] = useState([])

  useEffect(() => {
    axios.get(`http://www.omdbapi.com/?i=${episodeId}&apikey=8f4faf61`)
    .then((resp) => {
      setEpisode(resp.data)})
  }, [])

  return (
    <View style={styles.container}>
      <Text>{episode?.Title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }})
