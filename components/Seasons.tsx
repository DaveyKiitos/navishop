import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList, TextInput } from 'react-native';
import { StyleSheet, Text, View } from 'react-native';

import axios from 'axios'

export default function Seasons({ navigation }) {
  
  const [serieID, setSerieID] = useState()
  const [seasonCount, setSeasonCount] = useState(6)
  const [episodes, setEpisodes] = useState([]);

  useEffect(() => {
    for(let i = 0; i < seasonCount; i++)
    axios.get(`http://www.omdbapi.com/?i=${serieID}&apikey=8f4faf61&type=series&season=${i +1}`)
    .then((resp) => setEpisodes([...episodes, {resp.Episodes}]))
  },[search])

  return (
    <View style={styles.container}>
      <TextInput
        style={{ height: 40, borderColor: 'gray', borderWidth: 1, width: "90%" }}
        onChangeText={text => setSearch(text)}
        value={search}
      />
      <FlatList
        data={series}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}: {item: any}) => (
          <View>
            <Text onPress={() => navigation.push("Serie", {serieID: item.imdbID})}>
              {item.Title}
            </Text>
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }})
