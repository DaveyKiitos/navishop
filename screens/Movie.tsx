import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import FitImage from 'react-native-fit-image';
import axios from 'axios'

export default function Movie({route, navigation}) {
  console.log(route)
  const movieId = route.params?.movieID ;
  const [movie, setMovie] = useState([])

  useEffect(() => {
    axios.get(`http://www.omdbapi.com/?i=${movieId}&apikey=${global.apiToken}`)
    .then((resp) => {
      setMovie(resp.data)})
  }, [])

  return (
    <View style={styles.container}>
      <Text style={styles.movieTitle}>{movie?.Title}</Text>
      <FitImage
        source={{ uri: movie.Poster }}
        style={styles.moviePoster}
      />
      <Text>{movie.Plot}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  searchStyles: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: "90%"
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: "90%",
    marginHorizontal: "5%"
  },
  movieTitle: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  moviePoster: {
    width: 300,
    height: 500
  }
})