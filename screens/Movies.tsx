import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList, TextInput, View, Text } from 'react-native';
import axios from 'axios'
import FitImage from 'react-native-fit-image';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Movies({navigation}) {
  const [movies, setMovies] = useState([])
  const [search, setSearch] = React.useState('Star Wars');

  useEffect(() => {
    axios.get(`http://www.omdbapi.com/?s=${search}&apikey=${global.apiToken}&type=movie`)
    .then((resp) => setMovies(resp.data.Search))
  },[search])
  
  const MovieTeaser = ({item}: {item: any}) => (
    <TouchableOpacity
      style={styles.movieTeaser}  
      onPress={() => navigation.push("Movie", {movieID: item.imdbID})}
    >
      <Text style={styles.movieTitle}>{item.Title} ({item.Year})</Text>
      <FitImage
        source={{ uri: item.Poster }}
        style={styles.moviePoster}
      />
    </TouchableOpacity>
  )

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchStyles}
        onChangeText={text => setSearch(text)}
        value={search}
      />
      <FlatList
        data={movies}
        keyExtractor={(item, index) => index.toString()}
        renderItem={MovieTeaser}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  searchStyles: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: "90%"
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  movieTeaser: {
    alignItems: "center",
    backgroundColor: "lightgrey",
    width: "95%",
    marginTop: 25,
    paddingVertical: 25
  },
  movieTitle: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"

  },
  moviePoster: {
    width: 300,
    height: 500
  }
})
