import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';
import FitImage from 'react-native-fit-image';
import DropDownPicker from 'react-native-dropdown-picker';

import axios from 'axios'

export default function Movie({ route, navigation }) {
  const serieID = route.params?.serieID ;
  const [serie, setSerie] = useState([])
  const [season, setSeason] = useState(1)
  const [seasons, setSeasons] = useState(1)

  useEffect(() => {
    axios.get(`http://www.omdbapi.com/?i=${serieID}&apikey=${global.apiToken}`)
    .then((resp) => {
      setSerie(resp.data)})
  }, [])

  useEffect(() => {
    axios.get(`http://www.omdbapi.com/?i=${serieID}&apikey=${global.apiToken}&season=${season}`)
    .then((resp) => {
      setSeasons(resp.data.Episodes)})
  }, [season])

  const renderSeasons = () => {
    let seasons = []
    for (let i = 0; i < serie.totalSeasons; i++) {
      seasons.push({label: `Season ${i +1}`, value: i +1})
    }
    return seasons
  }

  const EpisodeTeaser = ({item}: {item: any}) => (
    <View style={styles.episodeContainer}>
      <Text>{item.Episode}</Text>
      <Text>{item.Title}</Text>
      <Text>{item.Released}</Text>
    </View>
  )

  return (
    
    <View style={styles.container}>
      <Text style={styles.serieTitle}>{serie?.Title}</Text>
      <FitImage
        source={{ uri: serie.Poster }}
        style={styles.seriePoster}
      />
      <Text>{serie.Plot}</Text>
      <DropDownPicker
        placeholder="Select a Season"
        items={renderSeasons()}
        containerStyle={{height: 40, width: 200}}
        style={{backgroundColor: '#fafafa'}}
        itemStyle={{justifyContent: 'flex-start'}}
        dropDownStyle={{backgroundColor: '#fafafa'}}
        onChangeItem={item => setSeason(item.value)}
      />

      <FlatList
        data={seasons}
        style={styles.seasonContainer}
        keyExtractor={(item, index) => index.toString()}
        renderItem={EpisodeTeaser}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  searchStyles: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: "90%"
  },
  seasonContainer: {
    width: "90%",
  },
  episodeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    textAlign: "center"
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: "90%",
    marginHorizontal: "5%"
  },
  serieTitle: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  seriePoster: {
    width: 300,
    height: 500
  }
})