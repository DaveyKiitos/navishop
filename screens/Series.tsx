import React, { useEffect, useState } from 'react';
import { StyleSheet, FlatList, TextInput, View, Text } from 'react-native';
import axios from 'axios'
import FitImage from 'react-native-fit-image';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function Series({navigation}) {
  const [series, setSeries] = useState([])
  const [search, setSearch] = React.useState('Breaking Bad');

  useEffect(() => {
    axios.get(`http://www.omdbapi.com/?s=${search}&apikey=${global.apiToken}&type=series`)
    .then((resp) => setSeries(resp.data.Search))
  },[search])
  
  const SerieTeaser = ({item}: {item: any}) => (
    <TouchableOpacity
      style={styles.serieTeaser}  
      onPress={() => navigation.push("Serie", {serieID: item.imdbID})}
    >
      <Text style={styles.serieTitle}>{item.Title} ({item.Year})</Text>
      <FitImage
        source={{ uri: item.Poster }}
        style={styles.seriePoster}
      />
    </TouchableOpacity>
  )

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchStyles}
        onChangeText={text => setSearch(text)}
        value={search}
      />
      <FlatList
        data={series}
        keyExtractor={(item, index) => index.toString()}
        renderItem={SerieTeaser}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  searchStyles: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    width: "90%"
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  serieTeaser: {
    alignItems: "center",
    backgroundColor: "lightgrey",
    width: "95%",
    marginTop: 25,
    paddingVertical: 25
  },
  serieTitle: {
    fontSize: 20,
    fontWeight: "bold"
  },
  seriePoster: {
    width: 300,
    height: 500
  }
})
