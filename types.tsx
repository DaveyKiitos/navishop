export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Movies: undefined;
  Series: undefined;
};

export type MoviesParamList = {
  Movies: undefined;
  Movie: undefined;
};

export type SeriesParamList = {
  Series: undefined;
  Serie: undefined;
};
